(function ($) {
	jQuery.support.cors = true;
    $(function () {
        initFormFunction('#main-form');
    });

    function initFormFunction(formId) {
        $(formId).validate({
            onkeyup: false,
            onclick: false,
            rules: {
	            userName: {
                    required: true,
                    minlength: 2
                },
	            userEmail: {
                    required: true,
                    email: true
                },
                subject: {
                    required: true,
                    minlength: 20
                }
            },
            messages: {
	            userName: {
                    required: "Заповніть поле!",
                    minlength: "Введіть мінімум 2 символи!"
                },
	            userEmail: {
                    required: "Заповніть поле!",
                    email: "Введено некоректний email"
                },
                subject: {
                    required: "Заповніть поле!",
                    minlength: "Введіть мінімум 20 символи!"
                }
            },
            highlight: function (element) {
                $(element).addClass('invalid');
            },
            unhighlight: function (element) {
                $(element).removeClass('invalid');
                $(element).addClass('valid');
            },
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    'type': 'POST',
                    'dataType': 'json',
                    'data': {},
                    'url': 'feedback/feedback-form.php',
                    'beforeSubmit': function (arr, $form, options) {
                    },
                    'success': console.log('success'),
                    'error': function (err) {
                        $(formId)
                            .addClass('invalid');
                    }
                });
            }

        });
    }

    $('input').on('keypress', function (event) {
        if (event.keyCode == 60 || event.keyCode == 62) {
            return false;
        }
    });


})(jQuery);

