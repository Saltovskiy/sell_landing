$(function () {
    $('.slider-section').slick({
        centerMode: true,
        centerPadding: '10%',
        slidesToShow: 1.665,
        autoplay: true,
        autoplaySpeed: 2000,
        prevArrow: '<button type="button" class="slick-prev"></button>',
        nextArrow: '<button type="button" class="slick-next"></button>',
        dots: true
    });




    function initializeMap($map) {
        var mapCenter = {lat: $map.data("center-lat"), lng: $map.data("center-lng")};
        var map = new google.maps.Map($map.get(0), {
            center: mapCenter,
            disableDefaultUI: true,
            zoom: $map.data("zoom"),
            zoomControl: true,
            scrollwheel: false,
            navigationControl: false,
            mapTypeControl: false,
            scaleControl: false,
            draggable: true,
        });

        var marker = new google.maps.Marker({
            position: {lat:mapCenter.lat, lng: mapCenter.lng},
            map: map,
            zIndex: 10
        });
    }

    initializeMap($('#map'));

    $("a").on("click", function (event) {
        if($(this).attr('data-scroll') === 'scroll'){
            event.preventDefault();
            var id  = $(this).attr('href');
            var top = $(id).offset().top;
            $('body,html').animate({scrollTop: top}, 1500);
        }
    });

});
