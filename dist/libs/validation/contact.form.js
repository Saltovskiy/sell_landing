(function ($) {

    $.validator.addMethod("characters", function (value, element) {
        return this.optional(element) || /^[а-яА-ЯЁёa-zA-Z]+$/i.test(value);
    });
    $.validator.addMethod("phoneVal", function (value, element) {
        return /^[\d\(\)\s\-\+]{19}$/.test(value);
    });
    $.validator.addMethod("emailValidation", function (value, element) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([а-яА-ЯЁёa-zA-Z\-0-9]+\.)+[а-яА-ЯЁёa-zA-Z]{2,}))$/;
        return re.test(value);
    });

    $(function () {
        initFooterForm('#footer-form');
    });

    function initFooterForm(formId) {
        $(formId).validate({
            onkeyup: false,
            onclick: false,
            rules: {
                userName: {
                    required: true,
                    minlength: 2,
                    characters: true
                },
                userPhone: {
                    required: true,
                    phoneVal: true
                }
            },
            messages: {
                userName: {
                    required: "Заполните поле!",
                    minlength: "Введите минимум 2 символа!",
                    characters: "Имя может содержать только буквы!"
                },
                userPhone: {
                    required: "Неверно заполнено поле!",
                    phoneVal: "Неверно заполнено поле!"
                }
            },
            highlight: function (element) {
                $(element).removeClass('valid');
                $(element).addClass('invalid');
            },
            unhighlight: function (element) {
                $(element).removeClass('invalid');
                if ($(element).val() != 0) {
                    $(element).addClass('valid');
                }
            },
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    'type': 'POST',
                    'data': {},
                    'url': 'index.php?route=account/edit/ajax_edit',
                    beforeSend: function () {
                        $(formId).find("button[type='submit']").prop("disabled", true);
                    },
                    complete: function () {
                        $(formId).find("button[type='submit']").prop("disabled", false);
                    },
                    success: function (result, statusText, xhr, form) {
                        if (result == "success") {
                            $('#footer-form').hide();
                            $('#footer h2').hide();
                            $('#hidden-form-wrap').show();
                        } else {

                        }
                    },
                    error: function (err) {

                    }
                });
            }
        });
    }




    // event.type должен быть keypress
    function getChar(event) {
        if (event.which == null) { // IE
            if (event.keyCode < 32) return null; // спец. символ
            return String.fromCharCode(event.keyCode)
        }

        if (event.which != 0 && event.charCode != 0) { // все кроме IE
            if (event.which < 32) return null; // спец. символ
            return String.fromCharCode(event.which); // остальные
        }

        return null; // спец. символ
    }

    $('input').on('keypress', function (event) {
        if (event.keyCode == 60 || event.keyCode == 62) {
            return false;
        }
    });

    $('.phone-input').mask("+38 (999) 99-99-999");


})(jQuery);

